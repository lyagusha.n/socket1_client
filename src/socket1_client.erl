-module(socket1_client).

-export([start/0]).

start() ->
    _ = application:ensure_all_started(socket1_client),
    ok.
