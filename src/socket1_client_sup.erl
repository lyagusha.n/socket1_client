-module(socket1_client_sup).
-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).

start_link() ->
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
	Procs = [
        #{id        => socket1_client_worker,
          start     => {socket1_client_worker, start_link, []},
          restart   => permanent,
          shutdown  => 5,
          type      => worker,
          modules   => [socket1_client_worker]}
    ],
	{ok, {{one_for_one, 1, 500}, Procs}}.
