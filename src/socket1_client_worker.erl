-module(socket1_client_worker).
-behaviour(gen_server).

-include("print.hrl").

%% API.
-export([start_link/0]).
-export([handle_event/1]).
-export([send/1]).
%% gen_server.
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

%% API.
-spec start_link() -> {ok, pid()}.
start_link() ->
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

send(Data) ->
    gen_server:cast(?MODULE, {send, Data}).

%% gen_server.
init([]) ->
    Host = application:get_env(socket1_client, host, "192.168.1.191"),
    Port = application:get_env(socket1_client, port, 9761),
    InitState = #{host => Host, port => Port, connect => 1},
    State = connect(InitState),
    {ok, State}.

handle_call(get_socket, _From, #{socket := Socket} = State) ->
	{reply, {ok, Socket}, State};
handle_call(_Request, _From, State) ->
	{reply, ignored, State}.

handle_cast({send, Data}, #{socket := Socket} = State) ->
    ok = gen_tcp:send(Socket, Data),
    {noreply, State};
handle_cast(_Msg, State) ->
	{noreply, State}.

handle_info(connect, State) ->
    {noreply, connect(State)};
handle_info({tcp_closed, _}, State) ->
    ok = log("socket closed"),
    {noreply, State};
handle_info({tcp, _, Data}, State) ->
    State1 = handle_event(Data, State),
    ET = erlang:system_time(seconds) + 5,
    {noreply, State1#{socket_expiry => ET}};
handle_info(update_socket_expiry, State) ->
    ET = erlang:system_time(seconds) + 5,
    {noreply, State#{socket_expiry => ET}};
handle_info(check_socket_expiry, #{socket_expiry := ET, socket := Socket} = State) ->
    Now = erlang:system_time(seconds),
    case ET > Now of
        true ->
            _ = gen_tcp:send(Socket, <<1>>),
            _ = timer:send_after(2500, check_socket_expiry),
            {noreply, State};
        false ->
            ok = gen_tcp:close(Socket),
            self()!connect,
            {noreply, State#{connect => 1}}
    end.

terminate(_Reason, _State) ->
	ok.

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

% intarnal
connect(#{host := Host, port := Port, connect := Try} = State) ->
    ok = log("try to connect(~p)...", [Try]),
    case gen_tcp:connect(Host, Port, [binary, {packet, 0}], 5000) of
        {ok, Sock}    ->
            ok = log("connected"),
            ET = erlang:system_time(seconds) + 5,
            ok = gen_tcp:send(Sock, <<50>>),
            self()!check_socket_expiry,
            State#{socket => Sock, socket_expiry => ET};
        {error, _Err} ->
            Sleep = case Try < 300 of
                true  -> Try * 1000;
                false -> 300000
            end,
            _ = timer:sleep(Sleep),
            self()!connect,
            State#{connect => Try+1}
    end.

log(Format) ->
    log(Format, []).

handle_event(Data, State) ->
    case Data of
        <<1, Rest/binary>> ->                                      % pong
            ET = erlang:system_time(seconds) + 5,
            handle_event(Rest, State#{socket_expiry => ET});
        <<3, _Device, _HVsn, _LVsn, _SoftType, Rest/binary>> ->    % device info
            handle_event(Rest, State);
        <<4, _HSN, _LSN, Rest/binary>> ->                          % serial number
            handle_event(Rest, State);
        <<15, Code, Rest/binary>> ->                               % badarg
            ok = log("handle error. code: ~p.", [Code]),
            handle_event(Rest, State);
        <<48, _Input, _OnOff, _ContactBounceTime, Rest/binary>> -> % input settings
            handle_event(Rest, State);
        <<49, Input, IState, Rest/binary>> ->                      % input open/close
            ParentHendler = application:get_env(socket1_client, parent_handler, ?MODULE),
            % TODO add device Id
            _ = ParentHendler:handle_event(<<49, Input, IState>>),
            OldHWState = maps:get(hardware_state, State, undefined),
            State1 = State#{hardware_state => OldHWState#{Input => IState}},
            handle_event(Rest, State1);
        <<50, I0, I1, I2, I3, Rest/binary>> = NewHWState ->        % state
            OldHWState = maps:get(hardware_state, State, undefined),
            State1 = case OldHWState == NewHWState of
                true  ->
                    State;
                false ->
                    ParentHendler = application:get_env(socket1_client, parent_handler, ?MODULE),
                    _ = ParentHendler:handle_event(<<50, I0, I1, I2, I3>>),
                    State#{hardware_state => #{0 => I0, 1 => I1, 2 => I2, 3 => I3}}
            end,
            handle_event(Rest, State1);
        <<>> ->
            State;
        _ ->
            ok = log("ignore event. data: ~p", [Data]),
            State
    end.

handle_event(Data) ->
    ok = log("handle event. data: ~p", [Data]),
%    ok = log("error. undefined parent handler", []).
    ok.

log(Format, Args) ->
    {{Y, M, D}, {H, Min, S}} = erlang:localtime(),
    Prefix = "~n~p/~p/~p ~p:~p:~p ",
    io:format(Prefix ++ Format, [D,M,Y,H,Min,S] ++ Args).
